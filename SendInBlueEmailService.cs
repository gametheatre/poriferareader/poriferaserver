﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using IdentityServer.Models;
using Task = System.Threading.Tasks.Task;

namespace IdentityServer
{
	public class SendInBlueEmailService : IEmailService
	{
		private readonly TransactionalEmailsApi apiInstance;
		private readonly UserStore users;
		private IWebHostEnvironment env;
		private bool hasKey;

		public SendInBlueEmailService(UserStore users, IWebHostEnvironment environment, SecretsService secrets)
		{
			this.apiInstance = new TransactionalEmailsApi();
			this.users = users;
			this.env = environment;
			this.hasKey = !string.IsNullOrWhiteSpace(secrets.SendInBlueApiKey);
		}

		public async Task SendPasswordRecoveryEmail(string address, HttpContext httpContext)
		{
			if (!this.hasKey) return;
			User user = await this.users.FindByEmail(address);
			SendSmtpEmail smtpEmail;
			if (user == null)
			{
				smtpEmail = new SendSmtpEmail()
				{
					Sender = new SendSmtpEmailSender("Porifera", "porifera-app@proton.me"),
					To = new List<SendSmtpEmailTo> { new SendSmtpEmailTo(address) },
					Subject = "Account access - Porifera",
					TextContent = "You (or someone else) attempted to use this email address to recover a password for access to a Porifera account. No account is registered to this email address. If you are expecting this email, it is possible that you signed up using a different email address, or that your signup was unsuccessful. If you did not ask for a password reset, please ignore this mail.",
				};
			}
			else
			{
				PasswordRecoveryAttempt attempt = this.users.GetNewRecoveryAttempt(user, httpContext);
				smtpEmail = new SendSmtpEmail()
				{
					Sender = new SendSmtpEmailSender("Porifera", "porifera-app@proton.me"),
					To = new List<SendSmtpEmailTo> { new SendSmtpEmailTo(address) },
					Subject = "Password recovery - Porifera",
					//TemplateId = 1,
					Params = new { Link = $"{httpContext.Request.Host}/Account/PasswordReset?Id={attempt.Id}" },
					TextContent = $"We received a password reset request for this account. \nIf you wish to reset your password use this link: https://{httpContext.Request.Host}/Account/PasswordReset?Id={attempt.Id}\n If you did not request this you can ignore this email",
				};
			}

			try
			{
				CreateSmtpEmail result = this.apiInstance.SendTransacEmail(smtpEmail);
				Debug.WriteLine(result);
			}
			catch (Exception e)
			{
				Debug.Print("Exception when calling TransactionalEmailsApi.SendTransacEmail: " + e.Message);
			}
		}

		public void SendConfirmationEmail(string address, string confirmationCode, HttpContext httpContext)
		{
			if (!this.hasKey || this.env.EnvironmentName != "Production") return;

			SendSmtpEmail sendSmtpEmail = new SendSmtpEmail()
			{
				Sender = new SendSmtpEmailSender("Porifera", "porifera-app@proton.me"),
				To = new List<SendSmtpEmailTo> { new SendSmtpEmailTo(address) },
				Subject = "Verify address - Porifera",
				//TemplateId = 2,
				Params = new { Link = $"{httpContext.Request.Host}/Account/VerifyEmailSuccess?Id={confirmationCode}" },
				TextContent = $"Someone signed up for a Porifera account using this email account. Click <a href=https://{httpContext.Request.Host}/Account/VerifyEmailSuccess?Id={confirmationCode}>this link</a> to verify your account. If this was not you, you can ignore this email"
			};
			try
			{
				// Send a transactional email
				CreateSmtpEmail result = this.apiInstance.SendTransacEmail(sendSmtpEmail);
				Debug.WriteLine(result);
			}
			catch (Exception e)
			{
				Debug.Print("Exception when calling TransactionalEmailsApi.SendTransacEmail: " + e.Message);
			}
		}

		public void SendResetConfirmationEmail(string address)
		{
			if (!this.hasKey) return;
			
			SendSmtpEmail smtpEmail = new SendSmtpEmail()
			{
				Sender = new SendSmtpEmailSender("Porifera", "porifera-app@proton.me"),
				To = new List<SendSmtpEmailTo> { new SendSmtpEmailTo(address) },
				Subject = "Password reset - Porifera",
				TextContent = "Your Porifera password has successfully been reset",
			};
			try
			{
				// Send a transactional email
				CreateSmtpEmail result = this.apiInstance.SendTransacEmail(smtpEmail);
				Debug.WriteLine(result);
			}
			catch (Exception e)
			{
				Debug.Print("Exception when calling TransactionalEmailsApi.SendTransacEmail: " + e.Message);
			}
		}

		public void SendTestEmail(string address = null)
		{
			if (!this.hasKey) return;
			
			SendSmtpEmail smtpEmail = new SendSmtpEmail()
			{
				Sender = new SendSmtpEmailSender("porifera-app@proton.me", "porifera-app@proton.me"),
				To = new List<SendSmtpEmailTo> { new SendSmtpEmailTo(address ?? "admin@porifera.site") },
				Subject = "Test",
				TextContent = "Test",
			};
			try
			{
				// Send a transactional email
				CreateSmtpEmail result = this.apiInstance.SendTransacEmail(smtpEmail);
				Debug.WriteLine(result);
			}
			catch (Exception e)
			{
				Debug.Print("Exception when calling TransactionalEmailsApi.SendTransacEmail: " + e.Message);
			}
		}
	}
}