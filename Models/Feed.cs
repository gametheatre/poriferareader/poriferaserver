using System;

namespace IdentityServer.Models;

public class Feed
{
	public Guid FeedId { get; set; }
	public string Content { get; set; }
	public Guid OwnerId { get; set; }
	public string Title { get; set; }
	public DateTime LastUpdated { get; set; }
	public TimeSpan ExpiryPeriod { get; set; }
}