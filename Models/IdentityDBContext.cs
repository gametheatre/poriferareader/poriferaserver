using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;

namespace IdentityServer.Models
{
	public class IdentityDBContext : DbContext
	{
		private readonly IWebHostEnvironment env;
		private readonly ILogger<IdentityDBContext> logger;
		private readonly SecretsService secrets;

		public DbSet<User> Users { get; set; }
		public DbSet<PasswordRecoveryAttempt> PasswordRecoveryAttempts { get; set; }

		public IdentityDBContext(IWebHostEnvironment env, SecretsService secrets, ILogger<IdentityDBContext> logger)
		{
			this.env = env;
			this.logger = logger;
			this.secrets = secrets;
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			Log.Information("Configuring DB");
			
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseNpgsql(this.secrets.ConnectionString);
			}
		}

		public DbSet<Feed> Feed { get; set; }
	}
}