﻿using System;

namespace IdentityServer.Models
{
	public partial class PasswordRecoveryAttempt
	{
		public Guid Id { get; set; }
		public DateTime TimeStamp { get; set; }
		public Guid User { get; set; }
		public string AccessFingerprint { get; set; }
	}
}