using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;

namespace IdentityServer;

public class SecretsService
{
	public string ConnectionString { get; }
	public string SendInBlueApiKey { get; }

	public SecretsService(IWebHostEnvironment env)
	{
		string secretsJson = File.ReadAllText("./secrets.json");
		JObject o = JObject.Parse(secretsJson);
		if (env.IsDevelopment())
		{
			this.ConnectionString = (string)o["ConnectionStringExternal"];
		}
		else
		{
			this.ConnectionString = (string)o["ConnectionStringInternal"];
		}
		
		this.SendInBlueApiKey = (string)o["SendInBlueKey"];
		
		if (!string.IsNullOrWhiteSpace(this.SendInBlueApiKey))
		{
			sib_api_v3_sdk.Client.Configuration.Default.ApiKey.Add("api-key", this.SendInBlueApiKey);
		}
	}
}