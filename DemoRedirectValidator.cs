﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IdentityServer
{
	// allows arbitrary redirect URIs - only for demo purposes. NEVER USE IN PRODUCTION
	public class DemoRedirectValidator : IRedirectUriValidator
	{
		static readonly Regex localRegex = new Regex(@"^https?://(localhost|127\.0\.0\.1):\d*/?$", RegexOptions.Compiled);

		public Task<bool> IsPostLogoutRedirectUriValidAsync(string requestedUri, Client client)
		{
			return Task.FromResult(true);
		}

		public Task<bool> IsRedirectUriValidAsync(string requestedUri, Client client)
		{
			bool result = client.RedirectUris.Contains(requestedUri);
			if (!result)
			{
				result = localRegex.IsMatch(requestedUri);
			}
			return Task.FromResult(result);
		}
	}
}