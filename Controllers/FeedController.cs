using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Models;
using IdentityServer4;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Headers;

namespace IdentityServer.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class FeedController : ControllerBase
	{
		private readonly IdentityDBContext context;

		public FeedController(IdentityDBContext context)
		{
			this.context = context;
		}

		// GET: api/Feed
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Feed>>> GetFeed()
		{
			await Console.Out.WriteLineAsync("GetFeeds");
			return await this.context.Feed.ToListAsync();
		}

		// GET: api/Feed/5
		[HttpGet("{id:guid}")]
		public async Task<ActionResult<string>> GetFeed(Guid id)
		{
			Feed feed = await this.context.Feed.FindAsync(id);

			if (feed == null)
			{
				return this.NotFound();
			}

			DateTime lastModifiedDate = feed.LastUpdated;
			if (lastModifiedDate < DateTime.UnixEpoch)
			{
				lastModifiedDate = DateTime.UnixEpoch;
			}

			RequestHeaders requestHeaders = this.HttpContext.Request.GetTypedHeaders();

			// HTTP does not provide milliseconds, so remove it from the comparison
			if (requestHeaders.IfModifiedSince.HasValue &&
			    lastModifiedDate.AddMilliseconds(-lastModifiedDate.Millisecond) <= requestHeaders.IfModifiedSince.Value)
			{
				return this.StatusCode((int)HttpStatusCode.NotModified);
			}

			ResponseHeaders responseHeaders = this.HttpContext.Response.GetTypedHeaders();
			responseHeaders.LastModified = lastModifiedDate;

			return new ContentResult
			{
				ContentType = "application/rss+xml",
				Content = feed.Content,
				StatusCode = 200
			};
		}

		// PUT: api/Feed/5
		// To protect from over posting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
		[Authorize(IdentityServerConstants.LocalApi.PolicyName)]
		[HttpPut("{id:guid}")]
		public async Task<IActionResult> PutFeed(Guid id, Feed feed)
		{
			var users = this.context.Users.Where(user => user.Email == "1@3").ToList();
			if (users.Any())
			{
				this.context.Users.Remove(users.First());
				await this.context.SaveChangesAsync();
			}

			await Console.Out.WriteLineAsync($"Update feed {id}");
			Guid userId = GetUserId(this.User);
			if (id != feed.FeedId)
			{
				return this.BadRequest();
			}

			Feed oldFeed = await this.context.Feed.FindAsync(id);

			if (oldFeed?.OwnerId != userId)
			{
				await Console.Out.WriteLineAsync($"db: {oldFeed?.OwnerId}, user {userId}");
				return this.Unauthorized();
			}

			feed.OwnerId = userId;

			this.context.Entry(oldFeed).CurrentValues.SetValues(feed);

			try
			{
				await this.context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!this.FeedExists(id))
				{
					return this.NotFound();
				}

				throw;
			}

			return this.Ok();
		}

		// POST: api/Feed
		// To protect from over posting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
		[Authorize(IdentityServerConstants.LocalApi.PolicyName)]
		[HttpPost]
		public async Task<ActionResult<Feed>> PostFeed([FromBody] Feed feed)
		{
			await Console.Out.WriteLineAsync($"Create feed {feed.FeedId}");
			if (this.FeedExists(feed.FeedId))
			{
				return this.BadRequest("Feed already exists");
			}

			if (feed.FeedId == default)
			{
				return this.BadRequest("invalid feed id");
			}

			Guid userId = GetUserId(this.User);

			feed.OwnerId = userId;
			await Console.Out.WriteLineAsync($"User {feed.OwnerId}");

			this.context.Feed.Add(feed);
			await this.context.SaveChangesAsync();

			return this.CreatedAtAction("GetFeed", new { id = feed.FeedId }, feed);
		}

		// DELETE: api/Feed/5
		[Authorize(IdentityServerConstants.LocalApi.PolicyName)]
		[HttpDelete("{id:guid}")]
		public async Task<IActionResult> DeleteFeed(Guid id)
		{
			await Console.Out.WriteLineAsync($"Delete feed {id}");
			Guid userId = GetUserId(this.User);
			Feed feed = await this.context.Feed.FindAsync(id);
			if (userId != feed?.OwnerId)
			{
				return this.Unauthorized();
			}

			this.context.Feed.Remove(feed);
			await this.context.SaveChangesAsync();

			return this.NoContent();
		}

		// MY FEEDS: api/Feed/MyFeeds
		[Authorize(IdentityServerConstants.LocalApi.PolicyName)]
		[HttpGet("MyFeeds")]
		public async Task<IActionResult> GetOwnFeeds()
		{
			Guid userId = GetUserId(this.User);
			await Console.Out.WriteLineAsync($"Get my feeds - {userId}");
			if (userId == default)
			{
				return this.NotFound();
			}

			List<Feed> results = this.context.Feed.Where(f => f.OwnerId == userId).ToList();

			return new ObjectResult(results);
		}

		private bool FeedExists(Guid id)
		{
			return this.context.Feed.Any(e => e.FeedId == id);
		}

		private static Guid GetUserId(ClaimsPrincipal user)
		{
			bool isUser =
				Guid.TryParse(
					user.Claims.SingleOrDefault(c =>
						c.Type == "sub")?.Value ?? "",
					out Guid userId);
			return userId;
		}
	}
}