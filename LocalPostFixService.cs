using System;
using System.Collections.Generic;
using System.Diagnostics;
using IdentityServer.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Task = System.Threading.Tasks.Task;

namespace IdentityServer;

// This email service is for linux servers with a local mail server running and MailUtils installed
public class LocalPostFixService : IEmailService
{
	private readonly UserStore users;
	private readonly IWebHostEnvironment env;
	private readonly ILogger logger;

	public LocalPostFixService(UserStore users, IWebHostEnvironment environment, ILogger<LocalPostFixService> logger)
	{
		this.users = users;
		this.env = environment;
		this.logger = logger;
	}

	public void SendConfirmationEmail(string address, string confirmationCode, HttpContext httpContext)
	{
		if (this.env.EnvironmentName != "Production") return;

		string from = "support@porifera.site";
		string subject = "Verify address - Porifera";
		string content = $"Someone signed up for a Porifera account using this email account. \n" +
		                 $"Use this link https://{httpContext.Request.Host}/Account/VerifyEmailSuccess?Id={confirmationCode} to verify your account. \n" +
		                 $"If this was not you, you can ignore this email ";
		this.SendEmail(from, address, content, subject);
	}

	public async Task SendPasswordRecoveryEmail(string address, HttpContext httpContext)
	{
		User user = await this.users.FindByEmail(address);
		if (user == null)
		{
			string from = "support@porifera.site";
			string subject = "Account access - Porifera";
			string content =
				$"You (or someone else) attempted to use this email address to recover a password for access to a Porifera account. \n" +
				$"No account is registered to this email address. \n" +
				$"If you are expecting this email, it is possible that you signed up using a different email address, or that your signup was unsuccessful. \n" +
				$"If you did not ask for a password reset, please ignore this mail.\n";
			this.SendEmail(from, address, content, subject);
		}
		else
		{
			PasswordRecoveryAttempt attempt = this.users.GetNewRecoveryAttempt(user, httpContext);

			string from = "support@porifera.site";
			string subject = "Password recovery - Porifera";
			string content = $"We received a password reset request for this account. \n" +
			                 $"If you wish to reset your password use this link: https://{httpContext.Request.Host}/Account/PasswordReset?Id={attempt.Id}\n " +
			                 $"If you did not request this you can ignore this email";
			this.SendEmail(from, address, content, subject);
		}
	}

	public void SendResetConfirmationEmail(string address)
	{
		string from = "support@porifera.site";
		string subject = "Password reset - Porifera";
		string content = $"Your Porifera password has successfully been reset";
		this.SendEmail(from, address, content, subject);
	}

	public void SendTestEmail(string address = null)
	{
		string from = "no-reply@porifera.site";
		string subject = "Test - Porifera";
		string content = $"This is a test email";
		this.SendEmail(from, address ?? "admin@porifera.site", content, subject);
	}

	private void SendEmail(string from, string to, string content, string subject)
	{
		content = content +
		          $"\n\n You can safely ignore rest of this email. \n" +
		          $"There are a lot of AIs on a lot of mail servers that will treat this message as though it is" +
		          $"not legitimate if it only has a link and a short explanation. This is because they are geared towards catering for big corporations" +
		          $"that want to tell you about 2% off their newest overpriced nonsense that no one needs. They fill their emails with a lot of text" +
		          $"and images full of marketing babble. In order to increase the chances of our emails being delivered" +
		          $"we therefore add this pretty pointless paragraph to all of them. We hope it does not disturb or annoy you, " +
		          $"but you can rest assured that not receiving your password reset link because some big corporation " +
		          $"decided you didn't need to see it would annoy you more." +
		          $"\n\n With love, your Porifera support team";
		this.logger.LogInformation("Sending email : {subject}", subject);
		using Process proc = new();
		proc.StartInfo.FileName = "/bin/bash";
		proc.StartInfo.Arguments = $"-c \"echo -e '{content}' | mail -aFrom:'{from}' -s '{subject}' '{to}'\"";
		proc.StartInfo.RedirectStandardOutput = true;
		proc.Start();
		string result = proc.StandardOutput.ReadToEnd();
		proc.WaitForExit();
		this.logger.LogInformation("Send mail attempt returned : {result}", result);
	}
}