﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityServer4.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace IdentityServerHost.Quickstart.UI
{
	[SecurityHeaders]
	[AllowAnonymous]
	public class HomeController : Controller
	{
		private readonly IIdentityServerInteractionService interaction;
		private readonly IWebHostEnvironment environment;
		private readonly ILogger logger;

		public HomeController(IIdentityServerInteractionService interaction, IWebHostEnvironment environment, ILogger<HomeController> logger)
		{
			this.interaction = interaction;
			this.environment = environment;
			this.logger = logger;
		}

		public IActionResult Index()
		{
			HomeViewModel vm = new ()
			{
				IsLoggedIn = this.User.Identity?.IsAuthenticated ?? false,
				UserName = this.User.Identity?.Name ?? string.Empty
			};
			return this.View(vm);
		}

		/// <summary>
		/// Shows the error page
		/// </summary>
		public async Task<IActionResult> Error(string errorId)
		{
			ErrorViewModel vm = new ErrorViewModel();

			// retrieve error details from identityserver
			IdentityServer4.Models.ErrorMessage message = await this.interaction.GetErrorContextAsync(errorId);
			if (message != null)
			{
				vm.Error = message;
			}

			return this.View("Error", vm);
		}
	}
}