namespace IdentityServerHost.Quickstart.UI;

public class HomeViewModel
{
	public bool IsLoggedIn { get; set; } = true;
	public string UserName { get; set; } = string.Empty;
}