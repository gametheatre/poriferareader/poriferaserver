using IdentityModel;
using IdentityServer;
using IdentityServer4;
using IdentityServer4.Events;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Models;

namespace IdentityServerHost.Quickstart.UI
{
	[SecurityHeaders]
	[AllowAnonymous]
	public class ExternalController : Controller
	{
		private readonly UserStore users;
		private readonly IIdentityServerInteractionService interaction;
		private readonly IClientStore clientStore;
		private readonly ILogger<ExternalController> logger;
		private readonly IEventService events;

		public ExternalController(
			IIdentityServerInteractionService interaction,
			IClientStore clientStore,
			IEventService events,
			ILogger<ExternalController> logger,
			UserStore users = null)
		{
			this.users = users;

			this.interaction = interaction;
			this.clientStore = clientStore;
			this.logger = logger;
			this.events = events;
		}

		/// <summary>
		/// initiate roundtrip to external authentication provider
		/// </summary>
		[HttpGet]
		public IActionResult Challenge(string scheme, string returnUrl)
		{
			if (string.IsNullOrEmpty(returnUrl)) returnUrl = "~/";

			// validate returnUrl - either it is a valid OIDC URL or back to a local page
			if (this.Url.IsLocalUrl(returnUrl) == false && this.interaction.IsValidReturnUrl(returnUrl) == false)
			{
				// user might have clicked on a malicious link - should be logged
				throw new Exception("invalid return URL");
			}

			// start challenge and roundtrip the return URL and scheme
			AuthenticationProperties props = new AuthenticationProperties
			{
				RedirectUri = this.Url.Action(nameof(this.Callback)),
				Items =
				{
					{ "returnUrl", returnUrl },
					{ "scheme", scheme },
				}
			};

			return this.Challenge(props, scheme);
		}

		/// <summary>
		/// Post processing of external authentication
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> Callback()
		{
			// read external identity from the temporary cookie
			AuthenticateResult result = await this.HttpContext.AuthenticateAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);
			if (result?.Succeeded != true)
			{
				throw new Exception("External authentication error");
			}

			if (this.logger.IsEnabled(LogLevel.Debug))
			{
				IEnumerable<string> externalClaims = result.Principal.Claims.Select(c => $"{c.Type}: {c.Value}");
				this.logger.LogDebug("External claims: {@claims}", externalClaims);
			}

			// lookup our user and external provider info
			(User user, string provider, string providerUserId, IEnumerable<Claim> claims) = await this.FindUserFromExternalProvider(result);
			if (user == null)
			{
				// Add this external user as a new user
				user = await this.AutoProvisionUser(provider, providerUserId, claims);
			}

			// this allows us to collect any additional claims or properties
			// for the specific protocols used and store them in the local auth cookie.
			// this is typically used to store data needed for signout from those protocols.
			List<Claim> additionalLocalClaims = new List<Claim>();
			AuthenticationProperties localSignInProps = new AuthenticationProperties();
			this.ProcessLoginCallback(result, additionalLocalClaims, localSignInProps);

			// issue authentication cookie for user
			IdentityServerUser isuser = new IdentityServerUser(user.UserId.ToString())
			{
				DisplayName = user.UserName,
				IdentityProvider = provider,
				AdditionalClaims = additionalLocalClaims
			};

			await this.HttpContext.SignInAsync(isuser, localSignInProps);

			// delete temporary cookie used during external authentication
			await this.HttpContext.SignOutAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);

			// retrieve return URL
			string returnUrl = result.Properties.Items["returnUrl"] ?? "~/";

			// check if external login is in the context of an OIDC request
			IdentityServer4.Models.AuthorizationRequest context = await this.interaction.GetAuthorizationContextAsync(returnUrl);
			await this.events.RaiseAsync(new UserLoginSuccessEvent(provider, providerUserId, user.UserId.ToString(), user.UserName, true, context?.Client.ClientId));

			if (context != null)
			{
				if (context.IsNativeClient())
				{
					// The client is native, so this change in how to
					// return the response is for better UX for the end user.
					return this.LoadingPage("Redirect", returnUrl);
				}
			}

			return this.Redirect(returnUrl);
		}

		private async Task<(User user, string provider, string providerUserId, IEnumerable<Claim> claims)> FindUserFromExternalProvider(AuthenticateResult result)
		{
			ClaimsPrincipal externalUser = result.Principal;

			// try to determine the unique id of the external user (issued by the provider)
			// the most common claim type for that are the sub claim and the NameIdentifier
			// depending on the external provider, some other claim type might be used
			Claim userIdClaim = externalUser.FindFirst(JwtClaimTypes.Subject) ??
							  externalUser.FindFirst(ClaimTypes.NameIdentifier) ??
							  throw new Exception("Unknown userid");

			// remove the user id claim so we don't include it as an extra claim if/when we provision the user
			List<Claim> claims = externalUser.Claims.ToList();
			claims.Remove(userIdClaim);

			string provider = result.Properties.Items["scheme"];
			string providerUserId = userIdClaim.Value;

			// find external user
			User user = await this.users.FindByExternalProvider(provider, providerUserId);

			return (user, provider, providerUserId, claims);
		}

		private async Task<User> AutoProvisionUser(string provider, string providerUserId, IEnumerable<Claim> claims)
		{
			User user = await this.users.AutoProvisionUser(provider, providerUserId, claims.ToList());
			return user;
		}

		// if the external login is OIDC-based, there are certain things we need to preserve to make logout work
		// this will be different for WS-Fed, SAML2p or other protocols
		private void ProcessLoginCallback(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
		{
			// if the external system sent a session id claim, copy it over
			// so we can use it for single sign-out
			Claim sid = externalResult.Principal.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
			if (sid != null)
			{
				localClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));
			}

			// if the external provider issued an id_token, we'll keep it for signout
			string idToken = externalResult.Properties.GetTokenValue("id_token");
			if (idToken != null)
			{
				localSignInProps.StoreTokens(new[] { new AuthenticationToken { Name = "id_token", Value = idToken } });
			}
		}
	}
}