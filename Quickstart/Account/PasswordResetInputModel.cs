﻿using System.ComponentModel.DataAnnotations;
using IdentityServer.Models;

namespace IdentityServerHost.Quickstart.UI
{
	public class PasswordResetInputModel
	{
		[Required]
		public string RecoveryAttemptId { get; set; }
		public PasswordRecoveryAttempt RecoveryAttempt { get; set; }

		[Required]
		[MinLength(6, ErrorMessage = "Password must be at least 6 characters long.")]
		public string Password { get; set; }

		[Required]
		[Compare(nameof(Password), ErrorMessage = "Passwords must be the same.")]
		public string ConfirmPassword { get; set; }
	}
}