// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer.Models;
using IdentityServer4;
using IdentityServer4.Events;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using IdentityServerHost.Quickstart.UI;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IdentityServer.Quickstart.Account
{
	/// <summary>
	/// This sample controller implements a typical login/logout/provision workflow for local and external accounts.
	/// The login service encapsulates the interactions with the user data store.
	/// The interaction service provides a way for the UI to communicate with identityserver for validation and context retrieval
	/// </summary>
	[SecurityHeaders]
	[AllowAnonymous]
	public class AccountController : Controller
	{
		private readonly UserStore users;
		private readonly IEmailService email;
		private readonly IIdentityServerInteractionService interaction;
		private readonly IClientStore clientStore;
		private readonly IAuthenticationSchemeProvider schemeProvider;
		private readonly IEventService events;
		private readonly UserValidator userValidator;
		private readonly ILogger logger;

		public AccountController(
			IIdentityServerInteractionService interaction,
			IClientStore clientStore,
			IAuthenticationSchemeProvider schemeProvider,
			IEventService events,
			UserValidator userValidator,
			UserStore users,
			IEmailService emailService,
			ILogger<AccountController> logger)
		{
			this.users = users;
			this.email = emailService;
			this.interaction = interaction;
			this.clientStore = clientStore;
			this.schemeProvider = schemeProvider;
			this.events = events;
			this.userValidator = userValidator;
			this.logger = logger;
		}

		/// <summary>
		/// Entry point into the sign up workflow
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> SignUp(string returnUrl)
		{
			// build a model so we know what to show on the sign up page
			SignUpInputModel vm = await this.BuildSignUpViewModelAsync(returnUrl);

			return this.View(vm);
		}

		/// <summary>
		/// Handle postback from username/password login
		/// </summary>
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SignUp(SignUpInputModel model)
		{
			// check if we are in the context of an authorization request
			AuthorizationRequest context = await this.interaction.GetAuthorizationContextAsync(model.ReturnUrl);

			if (this.ModelState.IsValid)
			{
				User user = await this.users.FindByEmail(model.Email);
				if (user != null)
				{
					await this.events.RaiseAsync(new UserLoginFailureEvent(model.Email, "username in use", clientId: context?.Client.ClientId));
					this.ModelState.AddModelError("inUse", "This email is already associated with an existing account");
					return this.View(model);
				}

				//Create the new user account
				user = this.users.Create(model.Email, model.Password);

				// issue authentication cookie with subject ID and username
				_ = new IdentityServerUser(user.UserId.ToString())
				{
					DisplayName = user.UserName
				};

				this.email.SendConfirmationEmail(model.Email, user.EmailConfirmationCode.ToString(), this.HttpContext);
				
				if (context != null)
				{
					await this.LogUserIn(context, model.Email, false);
					return this.Redirect(model.ReturnUrl);
				}

				return this.Redirect($"/Account/VerifyEmail?Email={model.Email}");
			}
			// something went wrong, show form with error
			return this.View(model);
		}

		/// <summary>
		/// Entry point into the login workflow
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> Login(string returnUrl)
		{
			// build a model so we know what to show on the login page
			LoginViewModel vm = await this.BuildLoginViewModelAsync(returnUrl);

			if (vm.IsExternalLoginOnly)
			{
				// we only have one option for logging in and it's an external provider
				return this.RedirectToAction("Challenge", "External", new { scheme = vm.ExternalLoginScheme, returnUrl });
			}

			return this.View(vm);
		}

		/// <summary>
		/// Handle postback from username/password login
		/// </summary>
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginInputModel model, string button)
		{
			// check if we are in the context of an authorization request
			AuthorizationRequest context = await this.interaction.GetAuthorizationContextAsync(model.ReturnUrl);

			// the user clicked the "cancel" button
			if (button != "login")
			{
				if (context != null)
				{
					// if the user cancels, send a result back into IdentityServer as if they
					// denied the consent (even if this client does not require consent).
					// this will send back an access denied OIDC error response to the client.
					await this.interaction.DenyAuthorizationAsync(context, AuthorizationError.AccessDenied);

					// we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
					if (context.IsNativeClient())
					{
						// The client is native, so this change in how to
						// return the response is for better UX for the end user.
						return this.LoadingPage("Redirect", model.ReturnUrl);
					}
					
					return this.Redirect(model.ReturnUrl);
				}
				else
				{
					// since we don't have a valid context, then we just go back to the home page
					return this.Redirect("~/");
				}
			}

			if (this.ModelState.IsValid)
			{
				// validate username/password against store
				if (await this.userValidator.ValidateAsync(model.Email, model.Password))
				{
					await this.LogUserIn(context, model.Email, model.RememberLogin);

					if (context != null)
					{
						if (context.IsNativeClient())
						{
							// The client is native, so this change in how to
							// return the response is for better UX for the end user.
							return this.LoadingPage("Redirect", model.ReturnUrl);
						}

						// we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
						return this.Redirect(model.ReturnUrl);
					}

					// request for a local page
					if (this.Url.IsLocalUrl(model.ReturnUrl))
					{
						return this.Redirect(model.ReturnUrl);
					}
					else if (string.IsNullOrEmpty(model.ReturnUrl))
					{
						return this.Redirect("~/");
					}
					else
					{
						// user might have clicked on a malicious link - should be logged
						throw new Exception("invalid return URL");
					}
				}

				await this.events.RaiseAsync(new UserLoginFailureEvent(model.Email, "invalid credentials", clientId: context?.Client.ClientId));
				this.ModelState.AddModelError(string.Empty, AccountOptions.InvalidCredentialsErrorMessage);
			}

			// something went wrong, show form with error
			LoginViewModel vm = await this.BuildLoginViewModelAsync(model);
			return this.View(vm);
		}

		/// <summary>
		/// Password recovery page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> RecoverPassword(string returnUrl)
		{
			await Task.CompletedTask;

			RecoveryInputModel vm = new()
			{
				ReturnUrl = returnUrl
			};

			return this.View(vm);
		}

		/// <summary>
		/// Handle recovery page postback
		/// </summary>
		[HttpPost]
		public async Task<IActionResult> RecoverPassword(RecoveryInputModel model, string button)
		{
			await Task.CompletedTask;
			if (button != "recover")
			{
				return this.Redirect(model.ReturnUrl);
			}

			if (this.ModelState.IsValid)
			{
				await this.email.SendPasswordRecoveryEmail(model.Email, this.HttpContext);
				return this.Redirect($"/Account/EmailSent/?email={model.Email}");
			}

			// something went wrong, show form with error
			RecoveryInputModel vm = new()
			{
				Email = model.Email,
				ReturnUrl = model.ReturnUrl
			};
			return this.View(vm);
		}

		/// <summary>
		/// Password recovery email sent page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> EmailSent(string emailAddress)
		{
			await Task.CompletedTask;

			EmailSentViewModel vm = new(emailAddress);

			return this.View(vm);
		}

		/// <summary>
		/// Password reset page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> PasswordReset(string id)
		{
			PasswordRecoveryAttempt attempt = this.users.ValidatePasswordReset(id, this.HttpContext);
			await Task.CompletedTask;

			PasswordResetInputModel vm = new()
			{
				RecoveryAttempt = attempt,
				RecoveryAttemptId = attempt?.Id.ToString()
			};

			return this.View(vm);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> PasswordReset(PasswordResetInputModel model, string button)
		{
			if (button == "reset")
			{
				//It is probably redundant to validate this again here, but redundancy in security is probably a good thing.
				model.RecoveryAttempt = this.users.ValidatePasswordReset(model.RecoveryAttemptId, this.HttpContext);

				if (this.ModelState.IsValid)
				{
					User user = await this.users.FindByUserId(model.RecoveryAttempt.User);
					if (model.RecoveryAttempt?.User == null)
					{
						return this.Redirect("~/Account/Login");
					}

					await this.users.ResetPassword(model.RecoveryAttempt, model.Password);

					this.email.SendResetConfirmationEmail(user.Email);

					return this.Redirect("~/Account/ResetSuccess");
				}
			}
			// something went wrong, show form with error
			return this.View(model);
		}

		/// <summary>
		/// Password reset success confirmation page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> ResetSuccess()
		{
			await Task.CompletedTask;

			ResetSuccessViewModel vm = new();

			return this.View(vm);
		}

		/// <summary>
		/// Password reset success confirmation page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> VerifyEmail(string emailAddress)
		{
			await Task.CompletedTask;

			VerifyEmailViewModel vm = new()
			{
				Email = emailAddress
			};

			return this.View(vm);
		}

		/// <summary>
		/// Password reset success confirmation page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> VerifyEmailSuccess(string id)
		{
			await Task.CompletedTask;

			VerifyEmailSuccessViewModel vm = new()
			{
				IsValid = await this.users.VerifyEmailCode(id)
			};

			return this.View(vm);
		}

		/// <summary>
		/// Show logout page
		/// </summary>
		[HttpGet]
		public async Task<IActionResult> Logout(string logoutId)
		{
			// build a model so the logout page knows what to display
			LogoutViewModel vm = await this.BuildLogoutViewModelAsync(logoutId);

			if (vm.ShowLogoutPrompt == false)
			{
				// if the request for logout was properly authenticated from IdentityServer, then
				// we don't need to show the prompt and can just log the user out directly.
				return await this.Logout(vm, "thing");
			}

			return this.View(vm);
		}

		/// <summary>
		/// Handle logout page postback
		/// </summary>
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Logout(LogoutInputModel model, string button)
		{
			if (button == "no")
			{
				return this.Redirect("~/");
			}

			// build a model so the logged out page knows what to display
			LoggedOutViewModel vm = await this.BuildLoggedOutViewModelAsync(model.LogoutId);

			if (this.User.Identity?.IsAuthenticated == true)
			{
				// delete local authentication cookie
				await this.HttpContext.SignOutAsync();

				// raise the logout event
				await this.events.RaiseAsync(new UserLogoutSuccessEvent(this.User.GetSubjectId(), this.User.GetDisplayName()));
			}

			// check if we need to trigger sign-out at an upstream identity provider
			if (vm.TriggerExternalSignout)
			{
				// build a return URL so the upstream provider will redirect back
				// to us after the user has logged out. this allows us to then
				// complete our single sign-out processing.
				string url = this.Url.Action("Logout", new { logoutId = vm.LogoutId });

				// this triggers a redirect to the external provider for sign-out
				return this.SignOut(new AuthenticationProperties { RedirectUri = url }, vm.ExternalAuthenticationScheme);
			}

			return this.View("LoggedOut", vm);
		}

		[HttpGet]
		public IActionResult AccessDenied()
		{
			return this.View();
		}

		/*****************************************/
		/* helper APIs for the AccountController */
		/*****************************************/

		private async Task<SignUpInputModel> BuildSignUpViewModelAsync(string returnUrl)
		{
			AuthorizationRequest context = await this.interaction.GetAuthorizationContextAsync(returnUrl);

			SignUpInputModel vm = new()
			{
				ReturnUrl = returnUrl,
				Email = context?.LoginHint,
			};

			this.logger.LogInformation("ReturnUrl: " + returnUrl);

			return vm;
		}

		private async Task<LoginViewModel> BuildLoginViewModelAsync(string returnUrl)
		{
			AuthorizationRequest context = await this.interaction.GetAuthorizationContextAsync(returnUrl);
			if (context?.IdP != null && await this.schemeProvider.GetSchemeAsync(context.IdP) != null)
			{
				bool local = context.IdP == IdentityServerConstants.LocalIdentityProvider;

				// this is meant to short circuit the UI and only trigger the one external IdP
				LoginViewModel vm = new()
				{
					EnableLocalLogin = local,
					ReturnUrl = returnUrl,
					Email = context.LoginHint,
				};

				if (!local)
				{
					vm.ExternalProviders = new[] { new ExternalProvider { AuthenticationScheme = context.IdP } };
				}

				return vm;
			}

			IEnumerable<AuthenticationScheme> schemes = await this.schemeProvider.GetAllSchemesAsync();

			List<ExternalProvider> providers = schemes
				.Where(x => x.DisplayName != null)
				.Select(x => new ExternalProvider
				{
					DisplayName = x.DisplayName ?? x.Name,
					AuthenticationScheme = x.Name
				}).ToList();

			bool allowLocal = true;
			if (context?.Client.ClientId != null)
			{
				Client client = await this.clientStore.FindEnabledClientByIdAsync(context.Client.ClientId);
				if (client != null)
				{
					allowLocal = client.EnableLocalLogin;

					if (client.IdentityProviderRestrictions != null && client.IdentityProviderRestrictions.Any())
					{
						providers = providers.Where(provider => client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme)).ToList();
					}
				}
			}

			return new LoginViewModel
			{
				AllowRememberLogin = AccountOptions.AllowRememberLogin,
				EnableLocalLogin = allowLocal && AccountOptions.AllowLocalLogin,
				ReturnUrl = returnUrl,
				Email = context?.LoginHint,
				ExternalProviders = providers.ToArray()
			};
		}

		private async Task<LoginViewModel> BuildLoginViewModelAsync(LoginInputModel model)
		{
			LoginViewModel vm = await this.BuildLoginViewModelAsync(model.ReturnUrl);
			vm.Email = model.Email;
			vm.RememberLogin = model.RememberLogin;
			return vm;
		}

		private async Task<LogoutViewModel> BuildLogoutViewModelAsync(string logoutId)
		{
			LogoutViewModel vm = new() { LogoutId = logoutId, ShowLogoutPrompt = AccountOptions.ShowLogoutPrompt };

			if (this.User.Identity?.IsAuthenticated != true)
			{
				// if the user is not authenticated, then just show logged out page
				vm.ShowLogoutPrompt = false;
				return vm;
			}

			LogoutRequest context = await this.interaction.GetLogoutContextAsync(logoutId);
			if (context?.ShowSignoutPrompt == false)
			{
				// it's safe to automatically sign-out
				vm.ShowLogoutPrompt = false;
				return vm;
			}

			// show the logout prompt. this prevents attacks where the user
			// is automatically signed out by another malicious web page.
			return vm;
		}

		private async Task<LoggedOutViewModel> BuildLoggedOutViewModelAsync(string logoutId)
		{
			// get context information (client name, post logout redirect URI and iframe for federated sign out)
			LogoutRequest logout = await this.interaction.GetLogoutContextAsync(logoutId);

			LoggedOutViewModel vm = new()
			{
				AutomaticRedirectAfterSignOut = AccountOptions.AutomaticRedirectAfterSignOut,
				PostLogoutRedirectUri = logout?.PostLogoutRedirectUri,
				ClientName = string.IsNullOrEmpty(logout?.ClientName) ? logout?.ClientId : logout.ClientName,
				SignOutIframeUrl = logout?.SignOutIFrameUrl,
				LogoutId = logoutId
			};

			if (this.User.Identity?.IsAuthenticated != true) return vm;
			string idp = this.User.FindFirst(JwtClaimTypes.IdentityProvider)?.Value;
			if (idp is null or IdentityServerConstants.LocalIdentityProvider) return vm;
			bool providerSupportsSignOut = await this.HttpContext.GetSchemeSupportsSignOutAsync(idp);
			if (!providerSupportsSignOut) return vm;
					
			// if there's no current logout context, we need to create one
			// this captures necessary info from the current logged in user
			// before we sign out and redirect away to the external IdP for sign out
			vm.LogoutId ??= await this.interaction.CreateLogoutContextAsync();

			vm.ExternalAuthenticationScheme = idp;

			return vm;
		}

		private async Task LogUserIn(AuthorizationRequest context, string emailAddress, bool rememberLogin)
		{
			User user = await this.users.FindByEmail(emailAddress);
			await this.events.RaiseAsync(new UserLoginSuccessEvent(user.UserName, user.UserId.ToString(), user.UserName, clientId: context?.Client.ClientId));

			// only set explicit expiration here if user chooses "remember me".
			// otherwise we rely upon expiration configured in cookie middleware.
			AuthenticationProperties props = null;
			if (AccountOptions.AllowRememberLogin && rememberLogin)
			{
				props = new AuthenticationProperties
				{
					IsPersistent = true,
					ExpiresUtc = DateTimeOffset.UtcNow.Add(AccountOptions.RememberMeLoginDuration)
				};
			}

			// issue authentication cookie with subject ID and username
			IdentityServerUser identityServerUser = new(user.UserId.ToString())
			{
				DisplayName = user.UserName
			};

			await this.HttpContext.SignInAsync(identityServerUser, props);
		}
	}
}
