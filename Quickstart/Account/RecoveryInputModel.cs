﻿using System.ComponentModel.DataAnnotations;

namespace IdentityServerHost.Quickstart.UI
{
	public class RecoveryInputModel
	{
		[Required]
		[EmailAddress]
		public string Email { get; set; }

		public string ReturnUrl { get; set; }
	}
}