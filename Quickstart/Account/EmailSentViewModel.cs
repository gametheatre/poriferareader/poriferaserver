﻿namespace IdentityServerHost.Quickstart.UI
{
	public class EmailSentViewModel
	{
		public string SentAddress { get; set; }

		public EmailSentViewModel(string sentAddress)
		{
			this.SentAddress = sentAddress;
		}
	}
}