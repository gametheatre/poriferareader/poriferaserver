using System.ComponentModel.DataAnnotations;

namespace IdentityServerHost.Quickstart.UI
{
	public class SignUpInputModel
	{
		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		[MinLength(6, ErrorMessage = "Password must be at least 6 characters long.")]
		public string Password { get; set; }

		[Required]
		[Compare(nameof(Password), ErrorMessage = "Passwords must be the same.")]
		public string ConfirmPassword { get; set; }

		public bool RememberLogin { get; set; }
		public string ReturnUrl { get; set; }
	}
}