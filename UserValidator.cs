﻿using IdentityModel;
using IdentityServer;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Models;
using IdentityServer.Utils;

public class UserValidator : IResourceOwnerPasswordValidator
{
	private readonly UserStore users;

	public UserValidator(UserStore users)
	{
		this.users = users;
	}

	public async Task<bool> ValidateAsync(string userName, string password)
	{
		ResourceOwnerPasswordValidationContext context =
			new ResourceOwnerPasswordValidationContext() { UserName = userName, Password = password };
		await this.ValidateAsync(context);
		return !context.Result.IsError;
	}

	public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
	{
		try
		{
			User user = await this.users.FindByEmail(context.UserName);
			if (user != null)
			{
				if (PasswordHash.VerifyHashedPassword(user.PasswordHash, context.Password))
				{
					context.Result = new GrantValidationResult(
						subject: user.UserId.ToString(),
						authenticationMethod: "custom",
						claims: GetUserClaims(user, this.users));

					return;
				}

				context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Incorrect password");
				return;
			}
			context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "User does not exist.");
			return;
		}
		catch
		{
			context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "Invalid username or password");
		}
	}

	//build claims array from user data
	public static Claim[] GetUserClaims(User user, UserStore users)
	{
		return new Claim[]
		{
			new Claim("user_id", user.UserId.ToString()),
			new Claim(JwtClaimTypes.Name, user.UserName),
			new Claim(JwtClaimTypes.Email, user.Email  ?? ""),
		};
	}
}