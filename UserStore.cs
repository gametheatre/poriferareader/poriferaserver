﻿using IdentityServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Utils;

namespace IdentityServer
{
	public class UserStore
	{
		private static readonly TimeSpan passwordResetExpiry = TimeSpan.FromMinutes(30);
		private readonly IdentityDBContext dbContext;

		public UserStore(IdentityDBContext dbContext)
		{
			this.dbContext = dbContext;
		}

		internal async Task<User> FindByEmail(string email)
		{
			return await this.dbContext.Users.FirstOrDefaultAsync(u => u.Email == email);
		}

#pragma warning disable IDE0060 // Remove unused parameter
		internal async Task<User> FindByExternalProvider(string provider, string providerUserId)
#pragma warning restore IDE0060 // Remove unused parameter
		{
			// TODO: if we add support for external providers
			await Task.CompletedTask;
			throw new NotImplementedException();
		}

#pragma warning disable IDE0060 // Remove unused parameter
		internal async Task<User> AutoProvisionUser(string provider, string providerUserId, List<Claim> lists)
#pragma warning restore IDE0060 // Remove unused parameter
		{
			// TODO: if we add support for external providers
			await Task.FromResult(0);
			throw new NotImplementedException();
		}

		internal async Task<User> FindByUserId(Guid guid)
		{
			return await this.dbContext.Users.FindAsync(guid);
		}

		internal User Create(string username, string password)
		{
			User user = new User()
			{
				UserId = Guid.NewGuid(),
				UserName = username,
				Email = username,
				PasswordHash = PasswordHash.HashPassword(password),
				EmailConfirmationCode = Guid.NewGuid(),
				CreationDate = DateTime.UtcNow
			};

			this.dbContext.Users.Add(user);
			this.dbContext.SaveChanges();

			return user;
		}

		internal PasswordRecoveryAttempt ValidatePasswordReset(string id, HttpContext httpContext)
		{
			if (!Guid.TryParse(id, out Guid attemptGuid))
			{
				return null;
			}

			PasswordRecoveryAttempt attempt = this.dbContext.PasswordRecoveryAttempts.Find(attemptGuid);
			if (attempt == null)
			{
				return null;
			}
			if (attempt.TimeStamp + passwordResetExpiry < DateTime.UtcNow)
			{
				return null;
			}

			return attempt;
		}

		internal PasswordRecoveryAttempt GetNewRecoveryAttempt(User user, HttpContext httpContext)
		{
			PasswordRecoveryAttempt result = new PasswordRecoveryAttempt()
			{
				Id = Guid.NewGuid(),
				TimeStamp = DateTime.UtcNow,
				User = user.UserId,
			};

			this.dbContext.PasswordRecoveryAttempts.Add(result);
			this.dbContext.SaveChanges();

			return result;
		}

		internal async Task ResetPassword(PasswordRecoveryAttempt attempt, string password)
		{
			User user = await this.FindByUserId(attempt.User);
			user.PasswordHash = PasswordHash.HashPassword(password);
			this.dbContext.PasswordRecoveryAttempts.Remove(attempt);
			this.dbContext.SaveChanges();
		}

		internal async Task<bool> VerifyEmailCode(string id)
		{
			User user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.EmailConfirmationCode.ToString().Equals(id));
			if (user != null)
			{
				user.EmailConfirmed = true;
				this.dbContext.SaveChanges();
				return true;
			}
			return false;
		}

		internal async Task DeleteUser(string email)
		{
			User user = await this.FindByEmail(email);
			if (user != null)
			{
				this.dbContext.Users.Remove(user);
				this.dbContext.SaveChanges();
			}
		}
	}
}