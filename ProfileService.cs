﻿using IdentityServer;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Models;

public class ProfileService : IProfileService
{
	private readonly ILogger<ProfileService> logger;
	private readonly UserStore users;

	public ProfileService(UserStore users, ILogger<ProfileService> logger)
	{
		this.logger = logger;
		this.users = users;
	}

	//Get user profile data in terms of claims when calling /connect/userinfo
	public async Task GetProfileDataAsync(ProfileDataRequestContext context)
	{
		try
		{
			//depending on the scope accessing the user data.
			if (!string.IsNullOrEmpty(context.Subject.Identity.Name))
			{
				//get user from db (in my case this is by email)
				User user = await this.users.FindByEmail(context.Subject.Identity.Name);

				if (user != null)
				{
					System.Security.Claims.Claim[] claims = UserValidator.GetUserClaims(user, this.users);

					//set issued claims to return
					context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
				}
			}
			else
			{
				//get subject from context (this was set UserValidator.ValidateAsync),
				//where and subject was set to my user id.
				System.Security.Claims.Claim userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "sub");

				Guid.TryParse(userId.Value, out Guid guid);

				if (guid != Guid.Empty)
				{
					//get user from db (find user by user id)
					User user = await this.users.FindByUserId(guid);

					// issue the claims for the user
					if (user != null)
					{
						System.Security.Claims.Claim[] claims = UserValidator.GetUserClaims(user, this.users);

						context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();
					}
				}
			}
		}
		catch (Exception ex)
		{
			this.logger.LogError(ex, ex.Message);
		}
	}

	//check if user account is active.
	public async Task IsActiveAsync(IsActiveContext context)
	{
		try
		{
			//get subject from context (set in ResourceOwnerPasswordValidator.ValidateAsync),
			System.Security.Claims.Claim userId = context.Subject.Claims.FirstOrDefault(x => x.Type == "user_id");

			Guid.TryParse(userId?.Value, out Guid guid);

			if (guid != Guid.Empty)
			{
				User user = await this.users.FindByUserId(guid);

				if (user != null)
				{
					//We don't currently have a mechanism for deactivating users
					context.IsActive = true;
				}
			}
		}
		catch (Exception ex)
		{
			this.logger.LogError(ex, ex.Message);
		}
	}
}