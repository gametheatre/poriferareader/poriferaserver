﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace IdentityServer
{
	public interface IEmailService
	{
		void SendConfirmationEmail(string address, string confirmationCode, HttpContext httpContext);
		Task SendPasswordRecoveryEmail(string address, HttpContext httpContext);
		void SendResetConfirmationEmail(string address);
		void SendTestEmail(string address = null);
	}
}