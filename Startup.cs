﻿using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using IdentityServer.Models;
using IdentityServer4;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer
{
	public class Startup
	{
		public IConfiguration Configuration { get; }
		public IWebHostEnvironment Environment { get; }

		public Startup(IWebHostEnvironment environment, IConfiguration configuration)
		{
			this.Environment = environment;
			this.Configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddScoped<UserStore>();
			services.AddScoped<UserValidator>();

			SecretsService secrets = new SecretsService(this.Environment);
			
			services.AddScoped<IEmailService, LocalPostFixService>();
			services.AddSingleton<SecretsService, SecretsService>(p => secrets);
			services.AddControllersWithViews();
			services.AddEntityFrameworkNpgsql().AddDbContext<IdentityDBContext>(opt => opt.UseNpgsql(secrets.ConnectionString));

			IIdentityServerBuilder builder = services.AddIdentityServer(options =>
				{
					// see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
					options.EmitStaticAudienceClaim = true;
				})
				.AddDeveloperSigningCredential()
				.AddInMemoryIdentityResources(Config.IdentityResources)
				.AddInMemoryClients(Config.Clients)
				.AddProfileService<ProfileService>()
				.AddInMemoryApiScopes(Config.GetApiScopes);

			services.AddTransient<IResourceOwnerPasswordValidator, UserValidator>();
			services.AddTransient<IProfileService, ProfileService>();
			services.AddLocalApiAuthentication();

			if (this.Environment.IsDevelopment())
			{
				builder.AddDeveloperSigningCredential();
			}

			services.AddTransient<IRedirectUriValidator, DemoRedirectValidator>();
			services.AddTransient<ICorsPolicyService, DemoCorsPolicy>();
			services.AddAuthorization(options =>
			{
				options.AddPolicy("PoriferaAPIPolicy", policy =>
				{
					policy.AddAuthenticationSchemes(IdentityServerConstants.LocalApi.AuthenticationScheme);
					policy.RequireAuthenticatedUser();
				});
			});
		}

		public void Configure(IApplicationBuilder app)
		{
			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});
			
			if (this.Environment.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHttpsRedirection();
			}

			app.UseHsts();

			app.UseStaticFiles();
			app.UseRouting();
			app.UseResponseCaching();

			app.UseIdentityServer();

			app.UseAuthentication();
			app.UseAuthorization();
			app.UseEndpoints(endpoints => { endpoints.MapDefaultControllerRoute(); });
		}
	}
}