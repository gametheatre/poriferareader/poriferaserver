﻿using System.Linq;

namespace IdentityServer.Utils
{
	public static class MathUtils
	{
		public static double Sum(params double[] values)
		{
			return values.Select(s => double.IsNaN(s) ? 0 : s).Sum();
		}
	}
}