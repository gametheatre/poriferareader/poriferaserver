﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace IdentityServer.Utils
{
	public static class StringUtils
	{
		public static string Sanitise(string s)
		{
			return string.Join("", s.AsEnumerable()
				.Select(chr => char.IsLetter(chr) || char.IsDigit(chr) || char.IsWhiteSpace(chr) || chr == '-' || chr == '!' || chr == '?' || chr == ',' || chr == ':' || chr == '/' || chr == '.'
					? chr.ToString()
					: "")
			);
		}

		public static string GetNextNumber(params string[] strings)
		{
			Regex regex = new Regex(@"\d*$", RegexOptions.Compiled);
			int highest = 1;
			foreach (string item in strings)
			{
				Match regexResult = regex.Match(item);
				if (!regexResult.Success)
				{
					continue;
				}

				if (int.TryParse(regexResult.Groups[0].Value, out int nextValue))
				{
					highest = Math.Max(highest, nextValue + 1);
				}
			}
			return highest.ToString();
		}
	}
}