﻿using System;
using System.Threading.Tasks;

namespace IdentityServer.Utils
{
	public static class ExceptionUtils
	{
		public static string GetaAllMessages(this Exception exp)
		{
			string message = string.Empty;
			Exception innerException = exp;

			do
			{
				message = message + (string.IsNullOrEmpty(innerException.Message) ? string.Empty : innerException.Message);
				innerException = innerException.InnerException;
			}
			while (innerException != null);

			return message;
		}

		/// <summary>
		/// If you mark a function with the [DebuggerStepThrough] attribute and then pass it to this function, you can disable that exception in this module thus supressing it in the debugger.
		/// Only works for functions that require no arguments and return a value
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		public static T OutsourceException<T>(Func<T> func)
		{
			T result = default;
			try
			{
				result = func.Invoke();
			}
			catch { }
			return result;
		}

		/// <summary>
		/// If you mark a function with the [DebuggerStepThrough] attribute and then pass it to this function, you can disable that exception in this module thus supressing it in the debugger.
		/// Only works for functions that require a single argument and return a value
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		public static T OutsourceException<U, T>(Func<U, T> func, U arg)
		{
			T result = default;
			try
			{
				result = func.Invoke(arg);
			}
			catch { }
			return result;
		}

		/// <summary>
		/// If you mark a function with the [DebuggerStepThrough] attribute and then pass it to this function, you can disable that exception in this module thus supressing it in the debugger.
		/// Only works for functions that require a single argument and has no return type
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		public static void OutsourceException<T>(Action<T> action, T arg)
		{
			try
			{
				action.Invoke(arg);
			}
			catch { }
		}

		/// <summary>
		/// If you mark a function with the [DebuggerStepThrough] attribute and then pass it to this function, you can disable that exception in this module thus supressing it in the debugger.
		/// Only works for functions that require two arguments and return a value
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		/// <returns></returns>
		public static async Task<V> OutsourceException<T, U, V>(Func<T, U, Task<V>> action, T arg, U arg2)
		{
			try
			{
				return await action.Invoke(arg, arg2);
			}
			catch { }
			return default;
		}
	}
}