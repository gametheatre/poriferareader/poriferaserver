﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityServer4.Models;
using System.Collections.Generic;
using IdentityServer4;

namespace IdentityServer
{
	public static class Config
	{
		public static IEnumerable<IdentityResource> IdentityResources =>
			new List<IdentityResource>
			{
				new IdentityResources.OpenId(),
				new IdentityResources.Profile(),
			};

		public static IEnumerable<ApiScope> GetApiScopes =>
			new ApiScope[]
			{
				new ApiScope(IdentityServerConstants.LocalApi.ScopeName, "Porifera API Server", new string[] { "name", "email", "user_id" }),
				new ApiScope("PoriferaAPI", "Porifera API Server", new string[] { "name", "email", "user_id" }),
			};
		
		public static IEnumerable<ApiResource> Apis = new List<ApiResource>
		{
			new ApiResource(IdentityServerConstants.LocalApi.ScopeName, "Porifera API Server", new string[] { "name", "email", "user_id" }),
		};

		public static IEnumerable<Client> Clients =>
			new List<Client>
			{
				new Client
				{
					ClientId = "desktopClient",

					// no interactive user, use the clientid/secret for authentication
					AllowedGrantTypes = GrantTypes.CodeAndClientCredentials,

					// secret for authentication
					ClientSecrets =
					{
						new Secret("a2b7e726-f88d-4994-9fbd-f84a8caebd7d".Sha256())
					},

					// where to redirect to after login
					RedirectUris = { "https://localhost" },

					// where to redirect to after logout
					PostLogoutRedirectUris = { "https://localhost:5002/signout-callback-oidc" },

					// scopes that client has access to
					AllowedScopes = { "openid", "profile", "PoriferaAPI", IdentityServerConstants.LocalApi.ScopeName },

					//AllowOfflineAccess = true,
					AccessTokenLifetime = 1814400,
					IdentityTokenLifetime = 1814400
				}
			};
	}
}